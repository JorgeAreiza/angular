import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { DestinoViaje } from '../models/destino-viaje.model';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud = 5;

  constructor(fb: FormBuilder) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      Nombre: ['', Validators.compose([Validators.required , this.NombreValidator , this.NombreValidatorParametrizable(this.minLongitud)])],
      URL: ['']
    });

    this.fg.valueChanges.subscribe((form: any) => {
      console.log('Cambio el formulario:', form );
      
    });
  }

  ngOnInit(): void {
  }

  Guardar(Nombre: string , URL: string): boolean{
    const D = new DestinoViaje(Nombre , URL);
    this.onItemAdded.emit(D);

    return false
  }

  NombreValidator(control: FormGroup): {[S: string]: boolean} {
    const L = control.value.toString().trim().length;
    if (L > 0 && L < 5) {
      return {invalidNombre: true}
    }
    return null;
  }

  NombreValidatorParametrizable(minLong: number): ValidatorFn{
    return (control: FormControl): {[S:string]: boolean} | null => {
      const L = control.value.toString().trim().length;
      if (L > 0 && L < minLong) {
        return {minLongNombre: true}
      }
      return null;
    }
  }
}
